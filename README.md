discord2wow
===========

Objective
---------

`players.yml`:

```yaml
---
raid_helper_users: Ferrus/BigBizzle, luc2, Gitankheal
discord_aliases:
  Ferrus/BigBizzle: Ferrus
  luc2: luc2
  Gitankheal: Zeze
players:
  Ferrus:
    - Bigbizzle
  luc2:
    - Luctrois
    - Bassocambo
    - Lucdeux
  Zeze:
    - Jtankill
    - Gitankheal
...
```

```
> discord2wow players.yml
1. Bigbizzle
2. Luctrois
3. Jtankill
Bigbizzle, Luctrois, Jtankill
...
```
