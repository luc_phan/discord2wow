import click
import yaml
from pathlib import Path


@click.command()
@click.argument('filename')
def cli(filename):
    command = Command()
    command.run(filename)


class Command:
    def run(self, filename):
        file = Path(filename)
        data = self._load(file)
        print(data)

        discord_users = data['raid_helper_users'].split(',')
        print(discord_users)

        cleaned_users = [user.strip() for user in discord_users]
        print(cleaned_users)

        filtered_users = [user for user in cleaned_users if user]
        print(filtered_users)

        # players = [data['discord_aliases'][user] for user in filtered_users]
        # print(players)

        players = list()
        unknowns = list()
        for user in filtered_users:
            if user not in data['discord_aliases']:
                print("*** Unknown player:", user)
                unknowns.append(user)
                continue
            players.append(data['discord_aliases'][user])
        print("Unknown players:", ', '.join(unknowns))
        print(players)

        wow_characters = [data['players'][player][0] for player in players]
        print(wow_characters)

        for i, character in enumerate(wow_characters, 1):
            print(i, character)

        print(', '.join(wow_characters))

    def _load(self, file):
        with file.open(encoding='utf-8') as f:
            return yaml.safe_load(f)
