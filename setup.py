from setuptools import setup

setup(
    name='discord2wow',
    version='0.1.0',
    packages=['discord2wow'],
    url='',
    license='',
    author='luc2',
    author_email='',
    description='',
    entry_points={'console_scripts': ['discord2wow = discord2wow.command:cli']},
    install_requires=['click', 'pyyaml']
)
